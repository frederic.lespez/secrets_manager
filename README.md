SECRETS_MANAGER.PY
==================

`secrets_manager.py` is a simple tool to manage secrets. It proceeds by replacing (in strings or files) tokens like this `@@@@SECRET_NAME@@@@` by the matching secret and printing the result to STDOUT.

Secrets are stored in a YAML file where each secret is encrypted (AES-256-GCM combined with Scrypt key derivation).

Token format is `@@@@SECRET_NAME@@@@` where `SECRET_NAME` is a string composed of uppercase and lowercase letters, numbers and the following characters `_-./` from the ASCII set.

If a token refers to a non existing secret, the secret will be automatically generated. By default all generated secrets are strings, 30 characters long, composed of uppercase and lowercase letters and numbers from the ASCII set.

Encryption/decryption use a master password stored in an environment variable named `SECRETS_MANAGER_MASTER_PASSWORD` by default.

## Dependencies:
* Click: https://pypi.org/project/click/
* PyCryptodome: https://pypi.org/project/pycryptodome/
* Scrypt: https://pypi.org/project/scrypt/
* PyYAML: https://pypi.org/project/PyYAML/

On Debian and its derivatives, these dependencies could be installed like this
```bash
apt install python3-click python3-pycryptodome python3-scrypt python3-yaml
```

## Usage

```bash
./secrets_manager.py --help
# Output
# Usage: secrets_manager.py [OPTIONS] COMMAND [ARGS]...

#   A simple tool to manage secrets.

#   It proceeds by replacing (in strings or files) tokens like this
#   `@@@@SECRET_NAME@@@@` by the matching secret and printing the result to
#   STDOUT.

# Options:
#   -s, --secrets PATH  Path to secrets file(default: secrets.yaml)
#   -e, --env TEXT      Environment variable containing the password for
#                       encrypting/decrypting secrets (default:
#                       SECRETS_MANAGER_MASTER_PASSWORD)

#   --help              Show this message and exit.

# Commands:
#   display         Display all secrets
#   get             Display the secret named NAME
#   process-file    Process INPUT_FILE(s) and send result to STDOUT
#   process-string  Process INPUT_STRING(s) and send result to STDOUT
#   set             Create a secret named NAME (a prompt will ask for the...
```

### Example - Processing strings
```bash
# Enter your master pasword
read -r -s -p "Enter your master password: " SECRETS_MANAGER_MASTER_PASSWORD; echo
export SECRETS_MANAGER_MASTER_PASSWORD
cat << 'EOF' > test_script.sh
#!/usr/bin/env bash
admin_password=$(./secrets_manager.py process-string @@@@admin@@@@)
# Now you could use the 'admin_password' in the rest of the script
echo "The secret is: ${admin_password}"
EOF
bash test_script.sh
# Output:
# The secret is: 6SGWVoHA08ZS67EO3Ygpk4iuiCCht2
cat secrets.yaml
# Output:
# admin: 2d25851bfa3abe79bd0071a9d7673b1e 3cad394ec51116e842a72a66161aa34ba51715cd5005f32b56720ad6300f
#   61faa9363dfa22a0c371644a0cf3f89a cee6eefda9ab36d39a77446820d67067
./secrets_manager.py display
# Output:
# admin = 6SGWVoHA08ZS67EO3Ygpk4iuiCCht2
```

### Example - Processing files

```bash
# Enter your master pasword
read -r -s -p "Enter your master password: " SECRETS_MANAGER_MASTER_PASSWORD; echo
export SECRETS_MANAGER_MASTER_PASSWORD
cat << 'EOF' > values.yaml
---
product1:
  username: admin
  password: @@@@product1_admin@@@@
product2:
  username: admin
  password: @@@@product2_admin@@@@
EOF
./secrets_manager.py process-file values.yaml
# Output
# ---
# product1:
#   username: admin
#   password: UvruQG8VNVQAfopSl5ZgwwNxvcgXmD
# product2:
#   username: admin
#   password: RgRTxbr2lxlVVJFdLjeWZC8LGQLT2x
cat secrets.yaml
# Output
# product1_admin: 346640884fe2fba269606472549007a0 687d3a69ec41aab7ffc1ef321ba0a3c877488f14af024047e32576c4378d
#   9cd938ae7788f942d58a88a336c862ed 4ab4b70f662ac7dc270dcf630c1fd6e2
# product2_admin: 83b4f8066b54bf51c6a7a951de90c01e 1e36564e465bc2ba972c91c732952d1f726743894b0733320caad40dda03
#   cbc71bf684ed68757d0ee644a99d781b 4c17456824201523a9b31723fe07a8e4
./secrets_manager.py display
# Output
# product1_admin = UvruQG8VNVQAfopSl5ZgwwNxvcgXmD
# product2_admin = RgRTxbr2lxlVVJFdLjeWZC8LGQLT2x
```

Useful with `kubectl` and `helm` commands
```bash
./secrets_manager.py process-file values.yaml|kubectl apply -f -
```
