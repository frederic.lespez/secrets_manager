#!/usr/bin/env python3

import binascii
import functools
import getpass
import os
import pathlib
import re
import secrets
import string

import click  # apt install python3-click
import Cryptodome.Cipher.AES  # apt install python3-pycryptodome
import scrypt  # apt install python3-scrypt
import yaml  # apt install python3-yaml

default_secrets_file_path = 'secrets.yaml'
default_master_password_env_var = "SECRETS_MANAGER_MASTER_PASSWORD"
secret_token_re = re.compile(r'@@@@([a-zA-Z0-9_./-]+)@@@@')
password_length = 40


# Encryption / decryption code from:
# https://cryptobook.nakov.com/symmetric-key-ciphers/aes-encrypt-decrypt-examples
def encrypt_AES_GCM(msg: bytes, password: bytes) -> tuple:
    kdfSalt = os.urandom(16)
    secretKey = scrypt.hash(password, kdfSalt, N=16384, r=8, p=1, buflen=32)
    aesCipher = Cryptodome.Cipher.AES.new(
        secretKey, Cryptodome.Cipher.AES.MODE_GCM)
    ciphertext, authTag = aesCipher.encrypt_and_digest(msg)
    return (kdfSalt, ciphertext, aesCipher.nonce, authTag)


def decrypt_AES_GCM(encryptedMsg: tuple, password: bytes) -> bytes:
    (kdfSalt, ciphertext, nonce, authTag) = encryptedMsg
    secretKey = scrypt.hash(password, kdfSalt, N=16384, r=8, p=1, buflen=32)
    aesCipher = Cryptodome.Cipher.AES.new(
        secretKey, Cryptodome.Cipher.AES.MODE_GCM, nonce)
    plaintext = aesCipher.decrypt_and_verify(ciphertext, authTag)
    return plaintext


def encrypt_secret(password, value: str) -> str:
    encrypted_msg = encrypt_AES_GCM(
        value.encode('utf-8'),
        password.encode('utf-8')
    )
    return ' '.join(
        [binascii.hexlify(item).decode('ascii') for item in encrypted_msg]
    )


def decrypt_secret(password, encrypted_value: str) -> str:
    encrypted_msg = tuple(
        binascii.unhexlify(item.encode('ascii'))
            for item in encrypted_value.split()
    )
    return decrypt_AES_GCM(
        encrypted_msg,
        password.encode('utf-8')
    ).decode('utf-8')


def generate_random_secret(length: int) -> str:
    # Only use letters and digits for password to avoid escaping or encoding
    # problems
    char_set = string.ascii_letters + string.digits
    return ''.join([secrets.choice(char_set) for i in range(length)])


def get_secret(name: str, secrets: dict[str, str], password_length: int) -> str:
    if name in secrets:
        value = secrets[name]
    else:
        value = generate_random_secret(password_length)
        secrets[name] = value
    return value


def load_secrets(path: pathlib.Path, password: str) -> dict[str, str]:
    secrets = {}
    if path.exists():
        with open(path) as f:
            secrets = yaml.load(f, Loader=yaml.loader.SafeLoader)
    for name, encrypted_value in secrets.items():
        secrets[name] = decrypt_secret(password, encrypted_value)
    return secrets


def save_secrets(path: pathlib.Path, secrets: dict[str, str], password: str) -> None:
    for name, value in secrets.items():
        secrets[name] = encrypt_secret(password, value)
    with open(path, 'w') as f:
        yaml.dump(secrets, f)


def replace_tokens(secrets: dict[str, str], line: str):
    """Replace tokens by secrets"""
    replace_token = functools.partial(
        get_secret,
        secrets=secrets, password_length=password_length,
    )
    return re.sub(secret_token_re, lambda m: replace_token(m.group(1)), line)


def validate_env_var(ctx, param, value):
    try:
        os.environ[value]
    except KeyError:
        raise click.BadParameter(f"Environment variable '{value}' must exist")
    return value


@click.group()
@click.option(
    '-s', '--secrets',
    'secrets_file_path',
    default=default_secrets_file_path,
    # type=click.Path(path_type=pathlib.Path),  # only with click v8.0
    type=click.Path(),
    help=f"Path to secrets file"
         f"(default: {default_secrets_file_path})",
)
@click.option(
    '-e', '--env',
    'master_password_env_var',
    default=default_master_password_env_var,
    callback=validate_env_var,
    type=str,
    help=f"Environment variable containing the password for "
         f"encrypting/decrypting secrets "
         f"(default: {default_master_password_env_var})",
)
@click.pass_context
def cli(ctx, secrets_file_path, master_password_env_var):
    """
    A simple tool to manage secrets.

    It proceeds by replacing (in strings or files) tokens like this
    `@@@@SECRET_NAME@@@@` by the matching secret and printing the result
    to STDOUT.
    """
    ctx.ensure_object(dict)
    ctx.obj['secrets_file_path'] = pathlib.Path(secrets_file_path)
    ctx.obj['master_password'] = os.environ[master_password_env_var]


@cli.command()
@click.pass_context
def display(ctx):
    """Display all secrets"""
    secrets_file_path = ctx.obj['secrets_file_path']
    master_password = ctx.obj['master_password']
    secrets = load_secrets(secrets_file_path, master_password)
    for k, v in secrets.items():
        print(f"{k} = {v}")


@cli.command()
@click.pass_context
@click.argument('name')
def get(ctx, name):
    """Display the secret named NAME"""
    secrets_file_path = ctx.obj['secrets_file_path']
    master_password = ctx.obj['master_password']
    secrets = load_secrets(secrets_file_path, master_password)
    print(f"{name} = {secrets[name]}")
    save_secrets(secrets_file_path, secrets, master_password)


@cli.command()
@click.pass_context
@click.argument('name')
def set(ctx, name):
    """Create a secret named NAME (a prompt will ask for the secret)"""
    secrets_file_path = ctx.obj['secrets_file_path']
    master_password = ctx.obj['master_password']
    secrets = load_secrets(secrets_file_path, master_password)
    secrets[name] = getpass.getpass(f"Enter the secret named '{name}':")
    save_secrets(secrets_file_path, secrets, master_password)


@cli.command()
@click.pass_context
@click.argument(
    'input_strings',
    type=str,
    nargs=-1,
)
def process_string(ctx, input_strings):
    """Process INPUT_STRING(s) and send result to STDOUT"""
    secrets_file_path = ctx.obj['secrets_file_path']
    master_password = ctx.obj['master_password']
    secrets = load_secrets(secrets_file_path, master_password)
    for input_string in input_strings:
        print(replace_tokens(secrets, input_string))
    save_secrets(secrets_file_path, secrets, master_password)


@cli.command()
@click.argument(
    'input_files',
    type=click.File(mode='r'),
    nargs=-1,
)
@click.pass_context
def process_file(ctx, input_files):
    """Process INPUT_FILE(s) and send result to STDOUT"""
    secrets_file_path = ctx.obj['secrets_file_path']
    master_password = ctx.obj['master_password']

    secrets = load_secrets(secrets_file_path, master_password)

    # Read all files, replace tokens by password and send result to STDOUT
    for input_file in input_files:
        for line in input_file:
            print(replace_tokens(secrets, line.rstrip()))

    save_secrets(secrets_file_path, secrets, master_password)


if __name__ == '__main__':
    cli(obj={})
